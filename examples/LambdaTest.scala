object LambdaTest {
  def execute(fun: () => Unit): Unit = {
    fun()
  }

  def apply(fun: (Int, Int) => Int, a: Int, b: Int): Int = {
    fun(a,b)
  }

  execute(fn () => {
    Std.printString("hello")
  });
  val mult: (Int, Int) => Int = fn (x, y) => {x*y};
  val plusOne: (Int) => Int = fn (x) => {x+1};
  val res0: Int = apply(mult, 3, 6);
  Std.printInt(res0);
  val res1: Int = plusOne(plusOne(1));
  Std.printInt(res1)
}
