// This example assumes we support non-closed anonymous functions, which is not
// (yet) the case.
object LambdaTest2 {
  def compose(f: (Int) => Int, g: (Int) => Int): (Int) => Int = {
    fn (x) => f(g(x))
  }


  val f1: (Int) => Int = fn (x) => x + 3;
  val f1: (Int) => Int = fn (x) => x - 2;
  val composedFunction: (Int) => Int = compose(f1, f2);
  Std.printInt(composedFunction(5))
}
