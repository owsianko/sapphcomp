object Directory {

  abstract class List

  case class Nil() extends List

  case class Cons(v: DirectoryObject, v: List) extends List

  abstract class DirectoryObject

  case class Group(v: String, v: List) extends DirectoryObject

  case class User(v: String, v: String) extends DirectoryObject

  def getGroupMembers(g: DirectoryObject): List = {
    g match {
      case Group(_, members) =>
        members
    }
  }

  def getName(o: DirectoryObject): String = {
    o match {
      case Group(name, _) =>
        name
      case User(name, _) =>
        name
    }
  }

  def contains(l: List, u: DirectoryObject): Boolean = {
    l match {
      case Nil() =>
        false
      case Cons(h, t) =>
        ((h == u) || contains(t, u))
    }
  }

  def addToGroup(group: DirectoryObject, user: DirectoryObject): DirectoryObject = {
    (if(contains(getGroupMembers(group), user)) {
      error("User already in Group!")
    } else {
      Group(getName(group), Cons(user, getGroupMembers(group)))
    })
  }

  def printStatus(group: DirectoryObject, user: DirectoryObject): Unit = {
    (if(contains(getGroupMembers(group), user)) {
      Std.printString((("Contains " ++ getName(user)) ++ "."))
    } else {
      Std.printString((("Do not contains " ++ getName(user)) ++ "."))
    })
  }

  val user1: DirectoryObject =
    User("Lamb Da", "void@domain.tld");
  val user2: DirectoryObject =
    User("Al Pha", "void@domain.tld");
  val members: DirectoryObject =
    Group("Members", Cons(user1, Nil()));
  Std.printString(("~ Working with group" ++ getName(members)));
  printStatus(members, user1);
  printStatus(members, user2);
  Std.printString("~ Updating group");
  val updatedMembers: DirectoryObject =
    addToGroup(members, user2);
  printStatus(updatedMembers, user1);
  printStatus(updatedMembers, user2)
}

