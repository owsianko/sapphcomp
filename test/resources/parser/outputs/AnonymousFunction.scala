object AnonymousFunction {
  val f: (Int, Int) => Int =
    fn (x, y) => {
      (x * y)
    };
  f(3, 4)
}

