object SortList {
  def getList(size: Int, acc: L.List): L.List = {
    Std.printString("Enter value: ");
    val input: Int = Std.readInt();

    if (size <= 1) {
      L.Cons(input, acc)
    } else {
      getList(size - 1, L.Cons(input, acc))
    }
  }

  Std.printString("~ Enter list:");
  val size: Int = 4;
  val l: L.List = getList(size, L.Nil());

  Std.printString("~ Sorted list:");
  Std.printString(L.toString(L.mergeSort(l)))
}
