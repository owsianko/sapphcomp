object Comments {
  // Single-line comment
  val a = 4; // Single-line comment

  /*
   * Multiline comment
   */
  a + 2
}
