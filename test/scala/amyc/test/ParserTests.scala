package amyc.test

import amyc.parsing._
import org.junit.Test

class ParserTests extends TestSuite with amyc.MainHelpers {
  val pipeline = Lexer andThen Parser andThen treePrinterN("")

  val baseDir = "parser"

  val outputExt = "scala"

  @Test def testEmpty = shouldOutput("Empty")
  @Test def testLiterals = shouldOutput("Literals")

  @Test def testEmptyFile = shouldFail("EmptyFile")

  // Extracted from the examples
  @Test def testArithmetic = shouldOutput("Arithmetic")
  @Test def testFactorial  = shouldOutput("Factorial")
  @Test def testHanoi      = shouldOutput("Hanoi")
  @Test def testHello      = shouldOutput("Hello")
  @Test def testHelloInt   = shouldOutput("HelloInt")
  @Test def testPrinting   = shouldOutput("Printing")
  @Test def testTestLists  = shouldOutput("TestLists")

  // Amy's librairies
  @Test def testList   = shouldOutput("List")
  @Test def testOption = shouldOutput("Option")
  @Test def testStd    = shouldOutput("Std")

  // Anonymous Functions
  @Test def testAnonymousFunction = shouldOutput("AnonymousFunction")
}

