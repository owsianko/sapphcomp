package amyc.test

import amyc.parsing._
import org.junit.Test

class LexerTests extends TestSuite {
  val pipeline = Lexer andThen DisplayTokens

  val baseDir = "lexer"

  val outputExt = "txt"

  @Test def testKeywords   = shouldOutput("Keywords")

  @Test def testSingleAmp  = shouldFail("SingleAmp")

  @Test def testUnclosedString  = shouldFail("UnclosedString")

  @Test def testFailingIntegerOverflow1  = shouldFail("IntegerOverflow1")
  @Test def testFailingIntegerOverflow2  = shouldFail("IntegerOverflow2")

  @Test def testPassingIntegerOverflow1  = shouldPass("IntegerOverflow1")
  @Test def testPassingIntegerOverflow2  = shouldPass("IntegerOverflow2")

  @Test def testComments = shouldOutput("Comments")

  // Extracted from the examples
  @Test def testArithmetic = shouldOutput("Arithmetic")
  @Test def testFactorial  = shouldOutput("Factorial")
  @Test def testHanoi      = shouldOutput("Hanoi")
  @Test def testHello      = shouldOutput("Hello")
  @Test def testHelloInt   = shouldOutput("HelloInt")
  @Test def testPrinting   = shouldOutput("Printing")
  @Test def testTestLists  = shouldOutput("TestLists")
}
