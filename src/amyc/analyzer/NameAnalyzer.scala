package amyc
package analyzer

import utils._
import ast.{Identifier, NominalTreeModule => N, SymbolicTreeModule => S}

// Name analyzer for Amy
// Takes a nominal program (names are plain strings, qualified names are string pairs)
// and returns a symbolic program, where all names have been resolved to unique Identifiers.
// Rejects programs that violate the Amy naming rules.
// Also populates and returns the symbol table.
object NameAnalyzer extends Pipeline[N.Program, (S.Program, SymbolTable)] {
  def run(ctx: Context)(p: N.Program): (S.Program, SymbolTable) = {
    import ctx.reporter._

    // Step 0: Initialize symbol table
    val table = new SymbolTable

    // Step 1: Add modules to table
    val modNames: Map[N.Name, List[N.ModuleDef]] = p.modules.groupBy(_.name)
    modNames.foreach {
      case (name, modules) =>
        if (modules.size > 1) {
          fatal(s"Two modules named $name in program", modules.head.position)
        }
    }

    modNames.keys.toList foreach table.addModule

    // Helper method: will transform a nominal type 'tt' to a symbolic type,
    // given that we are within module 'inModule'.
    def transformType(tt: N.TypeTree, inModule: String): S.Type = {
      tt.tpe match {
        case N.IntType     => S.IntType
        case N.BooleanType => S.BooleanType
        case N.StringType  => S.StringType
        case N.UnitType    => S.UnitType
        case N.ClassType(qn @ N.QualifiedName(module, name)) =>
          table.getType(module getOrElse inModule, name) match {
            case Some(symbol) =>
              S.ClassType(symbol)
            case None =>
              assert(tt.hasPosition)
              fatal(s"Could not find type $qn", tt)
          }
        case N.FunctionType(paramTypes, returnType) =>
          S.FunctionType(
            paramTypes.map(pt => transformType(N.TypeTree(pt), inModule)),
            transformType(N.TypeTree(returnType), inModule)
          )
      }
    }

    // Step 2: Check name uniqueness of definitions in each module
    val mods: Map[N.Name, Map[N.Name, List[N.ClassOrFunDef]]] =
      modNames
        .mapValues(l => l.head)
        .mapValues(modDef => modDef.defs)
        .mapValues(l => l.groupBy(def0 => def0.name));

    mods.foreach {
      case (modName, defs) =>
        defs.foreach {
          case (defName, defsWithName) =>
            if (defsWithName.size > 1) {
              fatal(s"Two Definitions named $defName in module $modName",
                    defsWithName.head.position);
            }
        }
    }

    // Step 3: Discover types and add them to symbol table
    val modsSingleDef: Map[N.Name, List[N.ClassOrFunDef]] =
      mods.mapValues(m => m.values.toList.flatten)

    modsSingleDef.foreach {
      case (modName, defsInMod) =>
        defsInMod.foreach(df =>
          df match {
            case N.AbstractClassDef(n) =>
              table.addType(modName, n)
            case _ => ()
        })
    };

    // Step 4: Discover type constructors, add them to table
    modsSingleDef.foreach {
      case (modName, defsInMod) =>
        defsInMod.foreach(df =>
          df match {
            case N.CaseClassDef(n, fields, parentName) =>
              val parent: Identifier =
                table.getType(modName, parentName) match {
                  case Some(id) => id
                  case None =>
                    fatal(s"Could not find type $parentName in $modName",
                          df.position)
                }
              table.addConstructor(modName,
                                   n,
                                   fields.map(tt => transformType(tt, modName)),
                                   parent)
            case _ => ()
        })
    };

    // Step 5: Discover functions signatures, add them to table
    modsSingleDef.foreach {
      case (modName, defsInMod) =>
        defsInMod.foreach(df =>
          df match {
            case N.FunDef(name, params, retType, _) =>
              table.addFunction(modName,
                                name,
                                params.map(p => transformType(p.tt, modName)),
                                transformType(retType, modName))
            case _ => ()
        })
    }

    // Step 6: We now know all definitions in the program.
    //         Reconstruct modules and analyse function bodies/ expressions

    // This part is split into three transfrom functions,
    // for definitions, FunDefs, and expressions.
    // Keep in mind that we transform constructs of the NominalTreeModule 'N'
    // to respective constructs of the SymbolicTreeModule 'S'.
    // transformFunDef is given as an example, as well as some code for the other ones

    def transformDef(df: N.ClassOrFunDef, module: String): S.ClassOrFunDef = {
      df match {
        case N.AbstractClassDef(name) =>
          val Some(id) = table.getType(module, name)
          S.AbstractClassDef(id).setPos(df);
        case N.CaseClassDef(name, fields, _) =>
          val Some((id, sig)) = table.getConstructor(module, name)
          if (fields.size != sig.argTypes.size) {
            fatal(s"Wrong number of arguments for constructor $name",
                  df.position)
          }
          S.CaseClassDef(id,
                         sig.argTypes
                           .map(t => S.TypeTree(t))
                           .zip(fields)
                           .map { case (tt0, tt1) => tt0.setPos(tt1) },
                         sig.parent)
        case fd: N.FunDef =>
          transformFunDef(fd, module)
      }
    }.setPos(df)

    def transformAnonFunDef(expr: N.AnonymousFunction, tpe: S.FunctionType)(
      implicit module: String): S.AnonymousFunction = {
      val S.FunctionType(paramTypes, returnType) = tpe
      val N.AnonymousFunction(paramNames, body) = expr

      if (paramTypes.size != paramNames.size) {
        fatal(s"Type signature does not match parameters: $expr",
          expr.position)
      }

      val sParams = paramTypes
        .zip(paramNames)
        .map({
          case (paramType, paramName) =>
            val sId = Identifier.fresh(paramName)
            val sTpe = S.TypeTree(paramType)
            S.ParamDef(sId, sTpe)
        })

        val (sId, numericId) = table.addAnonymousFunction(sParams.map(_.tt.tpe), returnType)

        val functionParamsMap = paramNames.zip(sParams.map(_.name)).toMap
        // TODO: support for non-closed functions
        val functionLocals: Map[String, Identifier] = Map()
        val functionEnv = (functionParamsMap, functionLocals)

        S.AnonymousFunction.fresh(
          sId,
          sParams.map(_.name),
          transformExpr(body)(module, functionEnv, None),
          numericId)
    }

    def transformFunDef(fd: N.FunDef, module: String): S.FunDef = {
      val N.FunDef(name, params, retType, body) = fd
      val Some((sym, sig)) = table.getFunction(module, name)

      params.groupBy(_.name).foreach {
        case (name, ps) =>
          if (ps.size > 1) {
            fatal(s"Two parameters named $name in function ${fd.name}", fd)
          }
      }

      val paramNames = params.map(_.name)
      val newParams = params zip sig.argTypes map {
        case (pd @ N.ParamDef(name, tt), tpe) =>
          val s = Identifier.fresh(name)
          S.ParamDef(s, S.TypeTree(tpe).setPos(tt)).setPos(pd)
      }
      val paramsMap = paramNames.zip(newParams).map({
        case (name, S.ParamDef(_, S.TypeTree(S.FunctionType(argTypes, retType)))) =>
          // Add anonymous functions to symbol table, to be used in name analysis
          // and type checking
          val (id, _) = table.addAnonymousFunction(argTypes, retType)
          (name, id)
        case (name, S.ParamDef(id, _)) => (name, id)
      }).toMap

      S.FunDef(
          sym,
          newParams,
          S.TypeTree(sig.retType).setPos(retType),
          transformExpr(body)(module, (paramsMap, Map()), None)
      ).setPos(fd)
    }

    def transformLiteral(lit: N.Literal[_]) = {
      lit match {
        case N.IntLiteral(i)     => S.IntLiteral(i)
        case N.BooleanLiteral(b) => S.BooleanLiteral(b)
        case N.StringLiteral(s)  => S.StringLiteral(s)
        case N.UnitLiteral()     => S.UnitLiteral()
      }
    }.setPos(lit)
    def getName(qname: N.QualifiedName, module: String) = {
      val owner = qname.module match {
        case Some(s) => s
        case None    => module
      }
      (owner, qname.name)
    }

    // This function takes as implicit a pair of two maps:
    // The first is a map from names of parameters to their unique identifiers,
    // the second is similar for local variables.
    // Make sure to update them correctly if needed given the scoping rules of Amy
    def transformExpr(expr: N.Expr)(
        implicit module: String,
        names: (Map[String, Identifier],
                Map[String, Identifier]),
        definedValParam: Option[S.Type]): S.Expr = {
      val (params, locals) = names

      def getVariableIden(name: String) = {
        (params.get(name), locals.get(name)) match {
          case (Some(_), Some(id)) => id
          case (None, Some(id))    => id
          case (Some(id), None)    => id
          case (None, None) =>
            fatal(s"Value $name undeclared in scope", expr.position)
        }
      }

      val res = expr match {
        case N.Match(scrut, cases) =>
          // Returns a transformed pattern along with all bindings
          // from strings to unique identifiers for names bound in the pattern.
          // Also, calls 'fatal' if a new name violates the Amy naming rules.
          def transformPattern(
              pat: N.Pattern): (S.Pattern, List[(String, Identifier)]) = {
            val (noPosPattern, names) = pat match {
              case N.WildcardPattern() => (S.WildcardPattern(), Nil)
              case N.IdPattern(name) =>
                if (locals contains name) {
                  fatal(s"Pattern identifier $name already defined",
                        pat.position)
                }
                table.getConstructor(module, name) match {
                  case Some((_, sig)) =>
                    val ctrString = sig.argTypes.mkString(", ");
                    warning(
                      s"Constructor $name($ctrString) exists; did you mean to use it?".stripMargin
                        .replaceAll("\n", " "),
                      pat.position);
                  case _ =>
                }
                val s = Identifier.fresh(name);
                (S.IdPattern(s), List((name, s)))

              case N.LiteralPattern(lit) =>
                (S.LiteralPattern(transformLiteral(lit)), Nil)
              case N.CaseClassPattern(qname, fields) =>
                val (owner, name) = getName(qname, module);
                val id = table.getConstructor(owner, name) match {
                  case Some((sym, sig)) =>
                    if (sig.argTypes.size != fields.size) {
                      fatal(
                        s"Case class $qname expects ${sig.argTypes.size} arguments, found ${fields.size}",
                        pat.position)
                    }
                    sym
                  case None =>
                    fatal(s"Could not find constructor $qname", pat.position)
                }
                val (newFields, newNames) = fields.map(transformPattern).unzip;
                val finalNames = newNames.flatten;
                finalNames.groupBy(_._1).foreach {
                  case (name, names) =>
                    if (names.size > 1) {
                      fatal(s"Duplicate name $name in pattern", pat.position)
                    }
                }
                (S.CaseClassPattern(id, newFields), finalNames)
            }
            (noPosPattern.setPos(pat), names)
          }

          def transformCase(cse: N.MatchCase) = {
            val N.MatchCase(pat, rhs) = cse
            val (newPat, moreLocals) = transformPattern(pat)
            val newExpr = transformExpr(rhs)(
              module,
              (params, locals ++ moreLocals),
              definedValParam)
            S.MatchCase(newPat, newExpr).setPos(cse)
          }

          S.Match(transformExpr(scrut), cases.map(transformCase))

        // Literals
        case lt: N.Literal[_] => transformLiteral(lt)

        // Binary Operations
        case N.Plus(lhs, rhs) => S.Plus(transformExpr(lhs), transformExpr(rhs))
        case N.Minus(lhs, rhs) =>
          S.Minus(transformExpr(lhs), transformExpr(rhs))
        case N.Times(lhs, rhs) =>
          S.Times(transformExpr(lhs), transformExpr(rhs))
        case N.Div(lhs, rhs) => S.Div(transformExpr(lhs), transformExpr(rhs))
        case N.Mod(lhs, rhs) => S.Mod(transformExpr(lhs), transformExpr(rhs))
        case N.LessThan(lhs, rhs) =>
          S.LessThan(transformExpr(lhs), transformExpr(rhs))
        case N.LessEquals(lhs, rhs) =>
          S.LessEquals(transformExpr(lhs), transformExpr(rhs))
        case N.And(lhs, rhs) => S.And(transformExpr(lhs), transformExpr(rhs))
        case N.Or(lhs, rhs)  => S.Or(transformExpr(lhs), transformExpr(rhs))
        case N.Equals(lhs, rhs) =>
          S.Equals(transformExpr(lhs), transformExpr(rhs))
        case N.Concat(lhs, rhs) =>
          S.Concat(transformExpr(lhs), transformExpr(rhs))

        // Unary Operations
        case N.Not(e) => S.Not(transformExpr(e))
        case N.Neg(e) => S.Neg(transformExpr(e))

        case N.Call(qname, args) =>
          val (owner, name) = getName(qname, module);

          val function = table.getFunction(owner, name)
          if (function.isDefined) {
            val (sym, sig) = function.get

            if (sig.argTypes.size != args.size) {
              fatal(
                s"Function $qname expects ${sig.argTypes.size} arguments, found ${args.size}",
                expr.position);
            }

            val transformedArgs = args.zip(sig.argTypes).map({
              case (argExpr, argType) =>
                transformExpr(argExpr)(module, names, Some(argType)).setPos(expr)
            })

            return S.Call(sym, transformedArgs).setPos(expr)
          }

          val constructor = table.getConstructor(owner, name)
          if (constructor.isDefined) {
            val (sym, sig) = constructor.get

            if (sig.argTypes.size != args.size) {
              fatal(
                s"Constructor $qname expects ${sig.argTypes.size} arguments, found ${args.size}",
                expr.position)
            }

            val transformedArgs = args.zip(sig.argTypes).map({
              case (argExpr, argType) =>
                transformExpr(argExpr)(module, names, Some(argType)).setPos(expr)
            })

            return S.Call(sym, transformedArgs).setPos(expr)
          }

          // Call to an anonymous function
          if (!(locals.contains(name) || params.contains(name))) {
            fatal(
              s"Could not find (anonymous) function or constructor $name in module $owner, $params",
              expr)
          }

          val symbol = locals.getOrElse(name, params(name))
          val sig = table
            .getAnonymousFunction(symbol)
            .getOrElse(
              fatal(s"Could not resolve anonymous function $name")
            )

            if (sig.argTypes.size != args.size) {
              fatal(
                s"Anonymous function $name expects ${sig.argTypes.size} arguments, found ${args.size}",
                expr.position)
            }

            S.AnonymousFunctionCall(symbol,
              args.map(transformExpr)).setPos(expr.position)

        case N.Sequence(e1, e2) =>
          S.Sequence(transformExpr(e1), transformExpr(e2))
        case N.Let(df, value, body) => {
          val s = Identifier.fresh(df.name)
          val sParamDef = S.ParamDef(
            s, S.TypeTree(transformType(df.tt, module)
              ).setPos(df.tt));

          if (locals contains df.name) {
            fatal(s"Value redefinition: ${df.name}", df.position);
          }

          if (params contains df.name) {
            warning(s"Value definition ${df.name} shadows function parameter",
                    df.position);
          }

          df.tt.tpe match {
            case N.FunctionType(_, _) =>
              value match {
                case fun@N.AnonymousFunction(_, _) =>
                  val function = transformExpr(fun.setPos(expr))(module, names, Some(transformType(df.tt, module))) match {
                    case sFun@S.AnonymousFunction(_, _, _, _) => sFun
                    // Can this case match? FIXME
                    case _ => fatal(s"Expected anonymous function, got $value", df.position)
                  }
                  val updatedTransformationEnv =
                    (params, locals.updated(df.name, function.id))
                  val transformedBody =
                    transformExpr(body)(module, updatedTransformationEnv, definedValParam)

                  S.Let(sParamDef, function, transformedBody)
                 case _ =>
                   fatal(s"Expected an anonymous function but got: $value",
                     value.position)
              }
            case _ =>
              S.Let(
                sParamDef,
                transformExpr(value),
                transformExpr(body)(
                  module,
                  (params, locals + (df.name -> s)), definedValParam))
          }
        }
        case N.Ite(cond, thenn, elze) =>
          S.Ite(transformExpr(cond), transformExpr(thenn), transformExpr(elze))
        case N.Error(msg) => S.Error(transformExpr(msg))
        case N.Variable(name) => {
          val iden = getVariableIden(name) 
          S.Variable(iden)
        }

        case anonFunDef@N.AnonymousFunction(_, _) => 
          val tpe = definedValParam.getOrElse(
            fatal(s"Could not resolve type of anonymous function: $expr", expr.position)
          )

          tpe match {
            case fTpe@S.FunctionType(_, _) =>
              transformAnonFunDef(anonFunDef, fTpe)
            case _ =>
              fatal(s"Non-fonction type for anonymous function: $expr", expr.position)
          }
      }



      res.setPos(expr)
    }

    // Putting it all together to construct the final program for step 6.
    val newProgram = S
      .Program(
        p.modules map {
          case mod @ N.ModuleDef(name, defs, optExpr) =>
            S.ModuleDef(
                table.getModule(name).get,
                defs map (transformDef(_, name)),
                optExpr map (transformExpr(_)(name, (Map(), Map()), None))
              )
              .setPos(mod)
        }
      )
      .setPos(p)

    (newProgram, table)

  }
}
